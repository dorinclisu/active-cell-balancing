/*
project: Active Cell Balancing
author: Dorin Clisu
description: This file implements the ADC reading 
*/
#ifndef _ADC16_H
#define _ADC16_H

#include <wirish.h> //STM32duino arduino compatibility

#define _N 256  //must be power of 2
//must also change << 8 to << log2(_N) in getRawVoltage()

class adc16
{
public:
	adc16(int Pin);
	void refresh();
	inline float getRawVoltage();
	inline float getVoltage();
	void calibrateP1(float Ref1);
	void calibrateP2(float Ref2);
private:
	int pin;

	int ring[_N];
	unsigned char head;  //int
	int sum;

	float ref1, ref2, mref1, mref2;
	float slope;
};


adc16::adc16(int Pin)
{
	pin = Pin;
	pinMode(pin, INPUT_ANALOG);
	
	for (head=_N-1; head>0; head--)
		ring[head] = 0;
	head = 0;
	sum = 0;
	mref1 = ref1 = 0.0;
	mref2 = ref2 = 3.3;
	slope = 1.0;
}

void adc16::refresh()
{
	int adc = analogRead(pin);
	sum += adc - ring[head];  //sum(n+1) = sum(n) + head - tail;
	ring[head] = adc;
	head++;  //char overflow at 255 wraps the buffer ends
	//head &= pow(2,_N);  
}

inline float adc16::getRawVoltage()  {return 3.3 * sum / (float)(4096 << 8);}
inline float adc16::getVoltage()     {return ref1 + (getRawVoltage() - mref1) * slope;}  //linear interpolation

void adc16::calibrateP1(float Ref1)
{
	ref1 = Ref1;
	for (int i=0; i<_N; i++)
	{
		refresh();
		delay(5);
	}
	mref1 = getRawVoltage();
	slope = (ref2 - ref1) / (mref2 - mref1);
}

void adc16::calibrateP2(float Ref2)
{
	ref2 = Ref2;
	for (int i=0; i<_N; i++)
	{
		refresh();
		delay(5);
	}
	mref2 = getRawVoltage();
	slope = (ref2 - ref1) / (mref2 - mref1);
}

#endif//_ADC16_H