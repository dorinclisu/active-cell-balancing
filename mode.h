/*
project: Active Cell Balancing
author: Dorin Clisu
description: This file implements the circuit currents and optimum mode computation
*/
#ifndef _MODE_H
#define _MODE_H

#define F_CLK_MHZ  72.0
#define NS_PER_CLK 1000.0 / F_CLK_MHZ
#define CYCLES_PER_US 72  //[MHz] 
#define L1 10  //[uH]

typedef struct
{	
	float Tc;
	float t1, t2, t3, t4;
	float d1, d2, d3, d4;
	bool sw1, sw2, sw3;
	float i1, i2, i3, i4, Ip;

} modeConfig_t;

modeConfig_t idle ={.Tc=1,
					.t1=0, .t2=0, .t3=0, .t4=0,
					.d1=0, .d2=0, .d3=0, .d4=0,
					.sw1=0, .sw2=0, .sw3=0};


void CDDD(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	m.t1 = 1 / v1;
	m.t2 = 1 / (v2 + v3 + v4);
	m.t3 = 1 / (v2 + v3 + v4);
	m.t4 = 1 / (v2 + v3 + v4);

	m.d1 = m.t2;
	m.d2 = 0;
	m.d3 = 0;
	m.d4 = 0;

	m.Tc = m.t2 + m.t1;

	m.t1 *= 0.8;  //avoid charging current reversal
	m.Tc *= 1.1;  //ensure discontinuous mode

	m.i1 =   m.t1 / m.Tc;  //skipped division by 2 !!!
	m.i2 = - m.t2 / m.Tc;
	m.i3 = - m.t3 / m.Tc;
	m.i4 = - m.t4 / m.Tc;

	m.sw1 = true;
	m.sw2 = false;
	m.sw3 = false;
}

void DCCC(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	m.t1 = 1 / v1;
	m.t2 = 1 / (v2 + v3 + v4);
	m.t3 = 1 / (v2 + v3 + v4);
	m.t4 = 1 / (v2 + v3 + v4);

	m.d1 = 0;
	m.d2 = m.t1;
	m.d3 = m.t1;
	m.d4 = m.t1;

	m.Tc = m.t1 + m.t2;

	m.t2 *= 0.8;
	m.t3 *= 0.8;
	m.t4 *= 0.8;
	m.Tc *= 1.1;

	m.i1 = - m.t1 / m.Tc;
	m.i2 =   m.t2 / m.Tc;
	m.i3 =   m.t3 / m.Tc;
	m.i4 =   m.t4 / m.Tc;

	m.sw1 = true;
	m.sw2 = false;
	m.sw3 = false;
}

void CDCC(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	m.t1 = 1 / v1;
	m.t2 = 2 / v2;
	m.t3 = 1 / (v3 + v4);
	m.t4 = 1 / (v3 + v4);

	m.d1 = m.t2;
	m.d2 = 0;
	m.d3 = m.t2;
	m.d4 = m.t2;

	m.Tc = m.t2 + m.t1;

	m.t1 *= 0.8;
	m.t3 *= 0.8;
	m.t4 *= 0.8;
	m.Tc *= 1.1;

	m.i1 =   m.t1 / m.Tc;
	m.i2 = - m.t2 / m.Tc;
	m.i3 =   m.t3 / m.Tc;
	m.i4 =   m.t4 / m.Tc;

	m.sw1 = true;
	m.sw2 = true;
	m.sw3 = false;
}

void DDCD(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	m.t1 = 1 / (v1 + v2);
	m.t2 = 1 / (v1 + v2);
	m.t3 = 2 / v3;
	m.t4 = 1 / v4;

	m.d1 = m.t4 - m.t1;
	m.d2 = m.t4 - m.t2;
	m.d3 = m.t4;
	m.d4 = 0;

	m.Tc = m.t4 + m.t3;

	m.t3 *= 0.8;
	m.Tc *= 1.1;

	m.i1 = - m.t1 / m.Tc;
	m.i2 = - m.t2 / m.Tc;
	m.i3 =   m.t3 / m.Tc;
	m.i4 = - m.t4 / m.Tc;

	m.sw1 = false;
	m.sw2 = true;
	m.sw3 = true;
}

void DDCC(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	m.t1 = 1 / (v1 + v2);
	m.t2 = 1 / (v1 + v2);
	m.t3 = 1 / (v3 + v4);
	m.t4 = 1 / (v3 + v4);

	m.d1 = 0;
	m.d2 = 0;
	m.d3 = m.t1;
	m.d4 = m.t1;

	m.Tc = m.t1 + m.t3;

	m.t3 *= 0.8;
	m.t4 *= 0.8;
	m.Tc *= 1.1;

	m.i1 = - m.t1 / m.Tc;
	m.i2 = - m.t2 / m.Tc;
	m.i3 =   m.t3 / m.Tc;
	m.i4 =   m.t4 / m.Tc;

	m.sw1 = false;
	m.sw2 = true;
	m.sw3 = false;
}

void DCDC(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	m.t1 = 1 / v1;
	m.t2 = 2 / v2;
	m.t3 = 2 / v3;
	m.t4 = 1 / v4;

	m.d1 = m.t3 - m.t1;
	m.d2 = m.t3;
	m.d3 = 0;
	m.d4 = m.t3;

	m.Tc = m.t3 + m.t2;

	m.t2 *= 0.8;
	m.t4 *= 0.8;
	m.Tc *= 1.1;

	m.i1 = - m.t1 / m.Tc;
	m.i2 =   m.t2 / m.Tc;
	m.i3 = - m.t3 / m.Tc;
	m.i4 =   m.t4 / m.Tc;

	m.sw1 = true;
	m.sw2 = true;
	m.sw3 = true;
}

void CDDC(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	m.t1 = 1 / v1;
	m.t2 = 2 / (v2 + v3);
	m.t3 = 2 / (v2 + v3);
	m.t4 = 1 / v4;

	m.d1 = m.t2;
	m.d2 = 0;
	m.d3 = 0;
	m.d4 = m.t2;

	if (m.t2 > m.t3)	m.Tc = m.t2;
	else				m.Tc = m.t3;

	if (m.t1 > m.t4)	m.Tc += m.t1;
	else				m.Tc += m.t4;

	m.t1 *= 0.8;
	m.t4 *= 0.8;
	m.Tc *= 1.1;

	m.i1 =   m.t1 / m.Tc;
	m.i2 = - m.t2 / m.Tc;
	m.i3 = - m.t3 / m.Tc;
	m.i4 =   m.t4 / m.Tc;

	m.sw1 = true;
	m.sw2 = false;
	m.sw3 = true;
}

void DCCD(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	m.t1 = 1 / v1;
	m.t2 = 2 / (v2 + v3);
	m.t3 = 2 / (v2 + v3);
	m.t4 = 1 / v4;

	m.d1 = 0;
	m.d2 = m.t1;
	m.d3 = m.t1;
	m.d4 = m.t1 - m.t4;

	if (m.t1 > m.t4)	m.Tc = m.t1;
	else				m.Tc = m.t4;

	if (m.t2 > m.t3)	m.Tc += m.t2;
	else				m.Tc += m.t3;

	m.t2 *= 0.8;
	m.t3 *= 0.8;
	m.Tc *= 1.1;

	m.i1 = - m.t1 / m.Tc;
	m.i2 =   m.t2 / m.Tc;
	m.i3 =   m.t3 / m.Tc;
	m.i4 = - m.t4 / m.Tc;

	m.sw1 = true;
	m.sw2 = false;
	m.sw3 = true;
}


inline void swap(float& a, float& b) {float tmp=a; a = b; b = tmp;}
inline void swap(bool& a, bool& b) {bool tmp=a; a = b; b = tmp;}

void mirrorConfig(modeConfig_t & m)
{
	swap(m.t1, m.t4);
	swap(m.t2, m.t3);

	swap(m.d1, m.d4);
	swap(m.d2, m.d3);

	swap(m.i1, m.i4);
	swap(m.i2, m.i3);

	swap(m.sw1, m.sw3);
}


void DDDC(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	CDDD(m, v4, v3, v2, v1);
	mirrorConfig(m);
}

void CCCD(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	DCCC(m, v4, v3, v2, v1);
	mirrorConfig(m);
}

void CCDC(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	CDCC(m, v4, v3, v2, v1);
	mirrorConfig(m);
}

void DCDD(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	DDCD(m, v4, v3, v2, v1);
	mirrorConfig(m);
}

void CCDD(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	DDCC(m, v4, v3, v2, v1);
	mirrorConfig(m);
}

void CDCD(modeConfig_t & m,  float v1, float v2, float v3, float v4)
{
	DCDC(m, v4, v3, v2, v1);
	mirrorConfig(m);
}




modeConfig_t getConfig(float v1, float v2, float v3, float v4)
{

	modeConfig_t m;

	void (*mode[14])	(modeConfig_t &, float, float, float, float)
	= {	CDDD, DCCC, CDCC, DDCD, DDCC, DCDC, CDDC, DCCD,
		DDDC, CCCD, CCDC, DCDD, CCDD, CDCD };

	int optimum;
	float min_error = 1000000;
	
	for (int i=0; i<14; i++)
	{
		(*mode[i]) (m, v1, v2, v3, v4);  //call function
		//paramteres are written to m

		float k = 0.001;
		float v = (v1 + v2 + v3 + v4) / 4;
		float error = \
			(v1 - v + k * m.i1) * (v1 - v + k * m.i1) + \
			(v2 - v + k * m.i2) * (v2 - v + k * m.i2) + \
			(v3 - v + k * m.i3) * (v3 - v + k * m.i3) + \
			(v4 - v + k * m.i4) * (v4 - v + k * m.i4);

		if (min_error > error)
		{
			min_error = error;
			optimum = i;
		}
	}
	
	(*mode[optimum]) (m, v1, v2, v3, v4);  //optimum configuration is written to m

	return m;
}



#endif //_MODE_H