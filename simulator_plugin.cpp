/*
project: Active Cell Balancing
author: Dorin Clisu
description: This file simulates the microcontroller algorithm by generating a pwm file in LTSpice format 
*/
#include <iostream>
#include <fstream>
using namespace std;

#include "mode.h"

#define CYCLES 2 

ofstream c1, c2, c3, c4, p1, p2, p3, p4, s1, s2, s3;

void setPWM(modeConfig_t m, float v1, float v2, float v3, float v4)
{
	c1<<"+20n\t"<< v1 <<endl;
	c2<<"+20n\t"<< v2 <<endl;
	c3<<"+20n\t"<< v3 <<endl;
	c4<<"+20n\t"<< v4 <<endl;

	s1<<"+20n\t"<< 3.3 * (1 - m.sw1) <<endl;
	s2<<"+20n\t"<< 3.3 * (1 - m.sw2) <<endl;
	s3<<"+20n\t"<< 3.3 * (1 - m.sw3) <<endl;

	p1<<"+"<< m.Tc + m.d1 <<"u\t3.3\n";
	p2<<"+"<< m.Tc + m.d2 <<"u\t3.3\n";
	p3<<"+"<< m.Tc + m.d3 <<"u\t3.3\n";
	p4<<"+"<< m.Tc + m.d4 <<"u\t3.3\n";

	for (int i=0; i<CYCLES; i++)
	{
		p1<<"+20n\t0.0\n";
		p1<<"+"<< m.t1 <<"u\t0.0\n";
		p1<<"+20n\t3.3\n";
		p1<<"+"<< m.Tc-m.t1 <<"u\t3.3\n";
		p1<<endl;

		p2<<"+20n\t0.0\n";
		p2<<"+"<< m.t2 <<"u\t0.0\n";
		p2<<"+20n\t3.3\n";
		p2<<"+"<< m.Tc-m.t2 <<"u\t3.3\n";
		p2<<endl;

		p3<<"+20n\t0.0\n";
		p3<<"+"<< m.t3 <<"u\t0.0\n";
		p3<<"+20n\t3.3\n";
		p3<<"+"<< m.Tc-m.t3 <<"u\t3.3\n";
		p3<<endl;

		p4<<"+20n\t0.0\n";
		p4<<"+"<< m.t4 <<"u\t0.0\n";
		p4<<"+20n\t3.3\n";
		p4<<"+"<< m.Tc-m.t4 <<"u\t3.3\n";
		p4<<endl;
	}

	p1<<"+"<< m.Tc-CYCLES*0.04 - m.d1 <<"u\t3.3\n";
	p2<<"+"<< m.Tc-CYCLES*0.04 - m.d2 <<"u\t3.3\n";
	p3<<"+"<< m.Tc-CYCLES*0.04 - m.d3 <<"u\t3.3\n";
	p4<<"+"<< m.Tc-CYCLES*0.04 - m.d4 <<"u\t3.3\n";

	s1<<"+"<< m.Tc*(CYCLES+2)-0.02 <<"u\t"<< 3.3 * (1 - m.sw1) <<endl;
	s2<<"+"<< m.Tc*(CYCLES+2)-0.02 <<"u\t"<< 3.3 * (1 - m.sw2) <<endl;
	s3<<"+"<< m.Tc*(CYCLES+2)-0.02 <<"u\t"<< 3.3 * (1 - m.sw3) <<endl;

	c1<<"+"<< m.Tc*(CYCLES+2)-0.02 <<"u\t"<< v1 <<endl;
	c2<<"+"<< m.Tc*(CYCLES+2)-0.02 <<"u\t"<< v2 <<endl;
	c3<<"+"<< m.Tc*(CYCLES+2)-0.02 <<"u\t"<< v3 <<endl;
	c4<<"+"<< m.Tc*(CYCLES+2)-0.02 <<"u\t"<< v4 <<endl;
}


int main()
{
	float Ipeak = 5.0;
	float scaling = Ipeak * L1;

	float volt[][4] = {
		{3.9, 4, 4, 4},
		{4, 3.9, 3.9, 3.9},
		{3.9, 4, 3.9, 3.9},
		{4, 4, 3.9, 4},
		{4, 4, 3.9, 3.9},
		{4, 3.9, 4, 3.9},
		{3.9, 4, 4, 3.9},
		{4, 3.9, 3.9, 4},
		{4, 4, 4, 3.9},
		{3.9, 3.9, 3.9, 4},
		{3.9, 3.9, 4, 3.9},
		{4, 3.9, 4, 4},
		{3.9, 3.9, 4, 4},
		{3.9, 4, 3.9, 4},
		{3.8, 3.9, 4, 4.2}
	};


	c1.open("V1.txt");  //create/open and append
	c2.open("V2.txt");
	c3.open("V3.txt");
	c4.open("V4.txt");
	p1.open("PWL1.txt");
	p2.open("PWL2.txt");
	p3.open("PWL3.txt");
	p4.open("PWL4.txt");
	s1.open("SW1.txt");
	s2.open("SW2.txt");
	s3.open("SW3.txt");

	for (int i=0; i<sizeof(volt)/(4*sizeof(float)); i++)
	{
		modeConfig_t m = getConfig(volt[i][0], volt[i][1], volt[i][2], volt[i][3]);

		float avg = (volt[i][0] + volt[i][1] + volt[i][2] + volt[i][3]) / 4;
		cout<< noshowpos<< volt[i][0] <<"  "<< volt[i][1] <<"  "<< volt[i][2] <<"  "<< volt[i][3] <<endl;

		for (int j=0; j<4; j++)
			if (volt[i][j] < avg)
				cout<< "+\t\t";
			else
				cout<< "-\t\t";
		cout<< endl;
		
		cout<< showpos << m.i1 <<"\t"<< m.i2 <<"\t"<< m.i3 <<"\t"<< m.i4 <<endl<<endl<<endl;

		m.Tc *= scaling;
		m.t1 *= scaling;
		m.t2 *= scaling;
		m.t3 *= scaling;
		m.t4 *= scaling;
		m.d1 *= scaling;
		m.d2 *= scaling;
		m.d3 *= scaling;
		m.d4 *= scaling;

		setPWM(m, volt[i][0], volt[i][1], volt[i][2], volt[i][3]);
	}

	p1.close();
	p2.close();
	p3.close();
	p4.close();
	s1.close();
	s2.close();
	s3.close();

	getchar();
	return 0;
}