/*
project: Active Cell Balancing
author: Dorin Clisu
description: This file is the main firmware file setting up the MCU timers, PWM and controlling the algorithm
*/
#include "adc16.h"
#include "mode.h"

#define VMIN 3.0
#define VMAX 4.2
#define IMAX 5.0
#define ERR_MAX 4*0.1*0.1

adc16 c1(D9);
adc16 c2(D8);
adc16 c3(D7);
adc16 c4(D6);

HardwareTimer pwm1(4);
HardwareTimer pwm2(2);
HardwareTimer pwm3(3);
HardwareTimer pwm4(1);

void setup()
{
//PWM SETUP

	TIMER1->regs.gen->CCER |= TIMER_CCER_CC1P;  //active low output polarity
	TIMER2->regs.gen->CCER |= TIMER_CCER_CC1P;
	TIMER3->regs.gen->CCER |= TIMER_CCER_CC1P;
	//TIMER4->regs.gen->CCER |= TIMER_CCER_CC1P;

	//USBSerial SerialUSB;
	//SerialUSB.end();

	pinMode(D27, PWM);  //timer1 ch1

	pinMode(D26, OUTPUT);  //timer1 ch2
	pinMode(D25, OUTPUT);  //timer1 ch3
	//pinMode(D24, OUTPUT);  //timer1 ch4 | USB
	pinMode(D22, OUTPUT);

	pinMode(D11, PWM);  //timer2 ch1
	//pinMode(D10, PWM);  //timer2 ch2
	//pinMode(D9, PWM);  //timer2 ch3
	//pinMode(D8, PWM);  //timer2 ch4
	pinMode(D5, PWM);  //timer3 ch1
	//pinMode(D4, PWM);  //timer3 ch2
	//pinMode(D3, PWM);  //timer3 ch3
	//pinMode(D33, PWM);  //timer3 ch4 | LED
	pinMode(D16, PWM);  //timer4 ch1
	//pinMode(D15, PWM);  //timer4 ch2
	//pinMode(D32, PWM);  //timer4 ch3 | BUTTON
	timer_set_compare(TIMER1, 1, 0);
	timer_set_compare(TIMER2, 1, 0);
	timer_set_compare(TIMER3, 1, 0);
	timer_set_compare(TIMER4, 1, 0);

	pwm1.refresh();
	pwm2.refresh();
	pwm3.refresh();
	pwm4.refresh();
	
	pwm1.pause();
	pwm2.pause();
	pwm3.pause();
	pwm4.pause();

	pwm1.setPrescaleFactor(1);
	pwm2.setPrescaleFactor(1);
	pwm3.setPrescaleFactor(1);
	pwm4.setPrescaleFactor(1);

	TIMER1->regs.gen->CR2 |= TIMER_CR2_MMS_ENABLE;  //master generate enable signal

	TIMER2->regs.gen->SMCR |= TIMER_SMCR_SMS_GATED;  //enabled in gated mode
	TIMER3->regs.gen->SMCR |= TIMER_SMCR_SMS_GATED;
	TIMER4->regs.gen->SMCR |= TIMER_SMCR_SMS_GATED;

	TIMER2->regs.gen->SMCR |= TIMER_SMCR_TS_ITR0;  //timer1 as enable signal source
	TIMER3->regs.gen->SMCR |= TIMER_SMCR_TS_ITR0;
	TIMER4->regs.gen->SMCR |= TIMER_SMCR_TS_ITR0;
	
	pwm1.resume();  //enabled, ready to start with timer1
	pwm2.resume();  //enabled, ready to start with timer1
	pwm3.resume();  //enabled, ready to start with timer1
	pwm4.resume();  //starts all timers at once
///PWM SETUP
	
	pinMode(D32, INPUT);
	
	Serial.begin(57600);
	Serial3.begin(57600);

	//c1.calibrateP1(4.0);
	//c2.calibrateP1(4.0);
	//c3.calibrateP1(4.0);
	//c4.calibrateP1(4.0);

	modeConfig_t m;
	m.Ip = 5.0;

	void (*mode[14])	(modeConfig_t &, float, float, float, float)
	= {	CDDD, DCCC, CDCC, DDCD, DDCC, DCDC, CDDC, DCCD,
		DDDC, CCCD, CCDC, DCDD, CCDD, CDCD };
	
	for (int i=0; i<14; i++)
	{
		(*mode[i]) (m, 4, 4, 4, 4);
		setPWM(m);
		delay(1);
	}
	setPWM(idle);

	//SerialUSB.begin();

    /*DEBUG MODE
	int refresh = 0;
	while (true)
	{
		c1.refresh();
		c2.refresh();
		c3.refresh();
		c4.refresh();

		refresh = (refresh+1) % 1024;
		delay(1);

		if (refresh == 0)
		{
			float v1 = 5.99 * c1.getRawVoltage();
			float v2 = 5.99 * c2.getRawVoltage() - v1;
			float v3 = 5.99 * c3.getRawVoltage() - v1 - v2;
			float v4 = 5.99 * c4.getRawVoltage() - v1 - v2 - v3;

			Serial3.print(v1);
			Serial3.print('\t');
			Serial3.print(v2);
			Serial3.print('\t');
			Serial3.print(v3);
			Serial3.print('\t');
			Serial3.println(v4);

			// m = getConfig(v1, v2, v3, v4);
			// m.Ip = 1;
			// setPWM(m);
		}

		if (Serial3.available())
		{
			int i = Serial3.read() - '0';
			while (Serial3.read() != ',')
				;

			m.Ip = Serial3.read() - '0';
			(*mode[i]) (m, 4, 4, 4, 4);
			setPWM(m);
		}
	}
	*/ //DEBUG MODE
}


void loop()
{
	c1.refresh();
	c2.refresh();
	c3.refresh();
	c4.refresh();

	static int refresh = 0;
	refresh = (refresh+1) % 1024;

	if (refresh == 0)
	{
		float v1 = c1.getVoltage();
		float v2 = c2.getVoltage() - v1;
		float v3 = c3.getVoltage() - v2;
		float v4 = c4.getVoltage() - v3;

		if ((VMIN<v1) and (v1<VMAX) and \
			(VMIN<v2) and (v2<VMAX) and \
			(VMIN<v3) and (v3<VMAX) and \
			(VMIN<v4) and (v4<VMAX))
		{
			modeConfig_t m = getConfig(v1, v2, v3, v4);
			float v = (v1 + v2 + v3 + v4) / 4;
			float error = (v1-v)*(v1-v) + (v2-v)*(v2-v) + (v3-v)*(v3-v) + (v4-v)*(v4-v);
			float duty = constrain(error / ERR_MAX, 0.0, 1.0);

			setPWM(m, duty);
		}
		else
			setPWM(idle);
	}	
}


bool safetyPass(modeConfig_t m)
{
	return true; //not implemented
}


void setPWM(modeConfig_t m, float duty=1)
{
	if (! safetyPass(m))
		return;

	m.Tc *= L1 * m.Ip;

	m.t1 *= L1 * m.Ip * duty;
	m.t2 *= L1 * m.Ip * duty;
	m.t3 *= L1 * m.Ip * duty;
	m.t4 *= L1 * m.Ip * duty;

	m.d1 *= L1 * m.Ip;
	m.d2 *= L1 * m.Ip;
	m.d3 *= L1 * m.Ip;
	m.d4 *= L1 * m.Ip;
	
	int period = m.Tc * CYCLES_PER_US;

	int width1 = m.t1 * CYCLES_PER_US;
	int width2 = m.t2 * CYCLES_PER_US;
	int width3 = m.t3 * CYCLES_PER_US;
	int width4 = m.t4 * CYCLES_PER_US;

	int delay1 = m.d1 * CYCLES_PER_US;
	int delay2 = m.d2 * CYCLES_PER_US;
	int delay3 = m.d3 * CYCLES_PER_US;
	int delay4 = m.d4 * CYCLES_PER_US;

	//turn off first
	timer_set_compare(TIMER1, 1, 0);
	timer_set_compare(TIMER2, 1, 0);
	timer_set_compare(TIMER3, 1, 0);
	timer_set_compare(TIMER4, 1, 0);
	
	pwm1.setOverflow(period);
	pwm2.setOverflow(period);
	pwm3.setOverflow(period);
	pwm4.setOverflow(period);

	pwm1.refresh();
	pwm2.refresh();
	pwm3.refresh();
	pwm4.refresh();

	pwm4.pause();  //pause all timers

	//pwm1.setCompare(1, width1);
	timer_set_compare(TIMER1, 1, width4);
	timer_set_compare(TIMER2, 1, width2);
	timer_set_compare(TIMER3, 1, width3);
	timer_set_compare(TIMER4, 1, width1);

	pwm1.setCount(period - delay1);
	pwm2.setCount(period - delay2);
	pwm3.setCount(period - delay3);
	pwm4.setCount(period - delay4);

	delay(1);

	digitalWrite(D26, ! m.sw1);
	digitalWrite(D22, ! m.sw2);
	digitalWrite(D25, ! m.sw3);

	delay(1);

	pwm4.resume();  //resume all timers
}